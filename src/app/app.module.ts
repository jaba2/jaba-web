import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MatSliderModule} from '@angular/material/slider';
import {MatStepperModule} from '@angular/material/stepper';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BusinessComponent} from './modules/business/business.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatNativeDateModule} from '@angular/material/core';
import {MaterialExampleModule} from 'material.module';
import {VendorService} from './services/vendor/vendor.service';
import {InvestmentComponent} from './modules/investment/investment.component';
import {AddInvestmentComponent} from './modules/investment/add-investment/add-investment.component';
import {CreateResourceDialogComponent} from './shared/dialogs/create-resource-dialog/create-resource-dialog.component';
import {ProductComponent} from './modules/product/product.component';
import {AddProductComponent} from "./modules/product/add-product/add-product.component";
import {CreateProductDialogComponent} from './shared/dialogs/create-product-dialog/create-product-dialog.component';
import {AddBusinessComponent} from "./modules/business/add-business/add-business.component";
import {CategoryService} from "./services/category/category.service";
import {PaymentTypeService} from "./services/payment-type/payment-type.service";
import {SubcategoryService} from "./services/subcategory/subcategory.service";
import {AlertDialogComponent} from './shared/dialogs/alert-dialog/alert-dialog.component';
import {JupiterTableBaseComponent} from './shared/components/jupiter-table-base/jupiter-table-base.component';
import {RouterModule} from "@angular/router";
import {SaleComponent} from './modules/sale/sale.component';
import {AddSaleComponent} from './modules/sale/add-sale/add-sale.component';
import {SearchProductDialogComponent} from './shared/dialogs/search-product-dialog/search-product-dialog.component';
import {DatePipe} from "@angular/common";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import { PaymentTypeSelectComponent } from './shared/components/payment-type-select/payment-type-select.component';
import { SaleDetailsDialogComponent } from './shared/dialogs/sale-details-dialog/sale-details-dialog.component';
import {CustomerService} from "./services/customer/customer.service";
import { ProductTagScanDialogComponent } from './shared/dialogs/product-tag-scan-dialog/product-tag-scan-dialog.component';
import { ExpenseComponent } from './modules/expense/expense.component';
import { AddExpenseComponent } from './modules/expense/add-expense/add-expense.component';
import { NavigationComponent } from './shared/components/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

@NgModule({
  declarations: [
    AppComponent,
    AddInvestmentComponent,
    AddBusinessComponent,
    AddProductComponent,
    BusinessComponent,
    InvestmentComponent,
    ProductComponent,
    CreateProductDialogComponent,
    CreateResourceDialogComponent,
    AlertDialogComponent,
    JupiterTableBaseComponent,
    SaleComponent,
    AddSaleComponent,
    SearchProductDialogComponent,
    PaymentTypeSelectComponent,
    SaleDetailsDialogComponent,
    ProductTagScanDialogComponent,
    ExpenseComponent,
    AddExpenseComponent,
    NavigationComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MatSliderModule,
    MatStepperModule,
    MaterialExampleModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule
  ],
  providers: [DatePipe, VendorService, CategoryService, PaymentTypeService, SubcategoryService, CustomerService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule {
}
