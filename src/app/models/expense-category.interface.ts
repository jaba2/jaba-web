import {Expense} from "./expense.interface";

export interface ExpenseCategory {
  expenseCategoryId: number;
  name: string;
  description: string;
  created: Date | string;
  updated: Date | string;
  deleted: boolean ;
  expenses: Expense[];
}
