export interface Customer {
  customerId?: number;
  name: string;
  phone?: string;
  email?: string;
}
