import {Vendor} from "./vendor.interface";
import {Investment} from "./investment.interface";
import {Product} from "./product.interface";

export interface Business {
  businessId?: number;
  name: string;
  description: string;
  investmentAmount?: number;
  created?: string;
  updated?: string;
  vendor: Vendor;
  investments: Investment[];
  sale?: any[];
  products: Product[];
}
