import {Category} from "./Category";

export interface Subcategory {
  subcategoryId: number;
  name: string;
  description?: string;
  category: Category;
}
