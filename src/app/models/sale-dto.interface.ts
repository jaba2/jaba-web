import {ProductSale} from "./ProductSale";
import {PaymentType} from "./payment-type.interface";
import {Customer} from "./customer.interface";

export interface SaleDTO {
  productsSale: ProductSale[];
  description: string;
  customer: Customer;
  paymentType: PaymentType;
}
