import {Sale} from "./sale.interface";
import {ProductTag} from "./product-tag.interface";

export interface SaleDetail {
  saleDetailsId?: number;
  description?: string;
  quantity?: number;
  amount?: number;
  totalAmount?: number;
  created?: Date | string;
  updated?: Date | string;
  deleted?: Boolean;
  sale?: Sale;
  productTags?: ProductTag[];
}
