export interface PaymentType {
  paymentTypeId: number;
  name: string;
  description: string;
}
