import {SaleDetail} from "./sale-detail.interface";

export interface ProductTag {
  productTagId?: number;
  serialNumber: string;
  groupQuantity?: number
  groupSequence?: number
  created?: Date | string;
  updated?: Date | string;
  deleted?: Boolean;
  saleDetail?: SaleDetail
}
