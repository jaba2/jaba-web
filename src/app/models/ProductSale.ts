import {ProductCode} from "./product-code.interface";
import {ProductTag} from "./product-tag.interface";

export interface ProductSale {
  product: string;
  description: string;
  quantity: number;
  totalQuantity: number;
  unitPrice: number;
  totalPrice: number;
  productCode: ProductCode;
  sku: string;
  productTags: ProductTag[];
}
