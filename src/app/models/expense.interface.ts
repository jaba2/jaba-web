import {PaymentType} from "./payment-type.interface";
import {ExpenseCategory} from "./expense-category.interface";

export interface Expense {
    expenseId?: number;
    name: string;
    amount: number;
    created?: string;
    updated?: string;
    deleted?: boolean ;
    paymentType: PaymentType;
    expenseCategory: ExpenseCategory;
}
