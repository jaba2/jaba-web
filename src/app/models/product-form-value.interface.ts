import {Subcategory} from "./subcategory.interface";
import {Category} from "./Category";
import {ProductCode} from "./product-code.interface";

export interface ProductFormValue {
  Id?: number;
  name: string;
  productCode: ProductCode;
  description: string;
  initialQuantity: number;
  quantity: number;
  purchasePrice: number;
  suggestedPrice: number;
  category: Category;
  subcategory: Subcategory;
}
