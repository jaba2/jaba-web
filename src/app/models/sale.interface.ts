import {Customer} from "./customer.interface";
import {PaymentType} from "./payment-type.interface";

export interface Sale {
   saleId: number;
   description: string;
   quantity: number;
   amount: number;
   created: Date | string;
   updated: Date | string;
   deleted: boolean;
   paymentType: PaymentType;
   customer: Customer;
}
