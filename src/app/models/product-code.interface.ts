import {Product} from "./product.interface";

export interface ProductCode {
  productCodeId?: number;
  code: string;
  description?: string;
  created?: Date | string;
  updated?: Date | string;
  deleted?: boolean;
  products?: Product[];
}
