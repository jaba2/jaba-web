import {Subcategory} from "./subcategory.interface";
import {ProductCode} from "./product-code.interface";

export interface Product {
  productId?: number;
  name?: string;
  productCode?: ProductCode;
  description?: string;
  initialQuantity?: number;
  quantity?: number;
  purchasePrice?: number;
  suggestedPrice?: number;
  subcategory?: Subcategory;
  population?: string
  flag?: string;
}
