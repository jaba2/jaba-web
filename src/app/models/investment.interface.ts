import { PaymentType } from './payment-type.interface';

export interface Investment {
  investmentId?: number;
  amount: number;
  description?: string;
  name: string;
  paymentType: PaymentType;
}
