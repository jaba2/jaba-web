import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BusinessComponent} from "./modules/business/business.component";
import {SaleComponent} from "./modules/sale/sale.component";
import {ExpenseComponent} from "./modules/expense/expense.component";

const routes: Routes = [
  { path: 'business', component: BusinessComponent },
  { path: 'sale', component: SaleComponent},
  { path: 'expenses', component: ExpenseComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
