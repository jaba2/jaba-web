import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PaymentType} from "../../../models/payment-type.interface";
import {PaymentTypeService} from "../../../services/payment-type/payment-type.service";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-payment-type-select',
  templateUrl: './payment-type-select.component.html',
  styleUrls: ['./payment-type-select.component.sass']
})
export class PaymentTypeSelectComponent implements OnInit {

  @Output() public paymentTypeSelected: EventEmitter<FormControl> = new EventEmitter<FormControl>();

  public paymentTypes: PaymentType[];
  public paymentTypeFormCtl: FormControl;

  constructor(
    private paymentTypeService: PaymentTypeService
  ) {
  }

  ngOnInit(): void {
    this.paymentTypeFormCtl = new FormControl([null, Validators.required]);
    this.getPaymentTypes();
  }

  public onSelectionChanges():void {
    this.paymentTypeSelected.emit(this.paymentTypeFormCtl)
  }

  private getPaymentTypes(): void {
    this.paymentTypeService.getPaymentTypes().subscribe({
      next: (paymentTypes: PaymentType[]) => this.paymentTypes = paymentTypes,
      error: (error) => console.error(error)
    });
  }
}
