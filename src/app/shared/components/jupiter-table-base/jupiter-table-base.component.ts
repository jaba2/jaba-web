import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTable} from "@angular/material/table";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {isEmpty as _isEmpty} from "lodash";
import {AlertDialogComponent} from "../../dialogs/alert-dialog/alert-dialog.component";

@Component({
  selector: 'app-jupiter-table-base',
  templateUrl: './jupiter-table-base.component.html',
  styleUrls: ['./jupiter-table-base.component.sass']
})
export class JupiterTableBaseComponent<T> {

  @ViewChild(MatTable) table: MatTable<T>;
  @Input() public data: T[];
  @Input() public allowAdd: boolean = true;
  @Input() public allowDelete: boolean = false;
  public dataSource: T[];

  constructor(private _tableDialog: MatDialog) {
  }

  protected initializeTable( displayedColumns: string[]): void {
    if (this.data && !_isEmpty(this.data)) {
      this.dataSource = this.data;
    } else {
      this.dataSource = [];
    }

    if (this.allowDelete) {
      displayedColumns.push('actions')
    }
  }

  protected showAlertDialog(): MatDialogRef<AlertDialogComponent> {
    return this._tableDialog.open(AlertDialogComponent, {
      width: '300px'
    });
  }

}
