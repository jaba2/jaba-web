import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JupiterTableBaseComponent } from './jupiter-table-base.component';

describe('JupiterTableBaseComponent', () => {
  let component: JupiterTableBaseComponent;
  let fixture: ComponentFixture<JupiterTableBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JupiterTableBaseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JupiterTableBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
