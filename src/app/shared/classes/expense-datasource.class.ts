import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Expense} from "../../models/expense.interface";
import {BehaviorSubject, Observable} from "rxjs";
import {ExpenseService} from "../../services/expense/expense.service";
import {Page} from "../../models/paginator/page.interface";

export class ExpenseDataSource implements DataSource<Expense> {
  private expensesSubject = new BehaviorSubject<Expense[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalElements: number;

  constructor(private expenseService: ExpenseService) {
    this.totalElements = 0;
  }

  public connect(collectionViewer: CollectionViewer): Observable<Expense[]> {
    return this.expensesSubject.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    this.expensesSubject.complete();
    this.loadingSubject.complete();
  }

  public add(expense: Expense): void {
    const expenses: Expense[] = this.expensesSubject.getValue();
    expenses.unshift(expense);
    this.expensesSubject.next(expenses);
    this.totalElements++;
  }

  public loadExpenses(page: number, size: number, pattern: string, sort: string) {
    this.loadingSubject.next(true);

    this.expenseService.getAll(page, size, pattern, sort)
      .subscribe({
        next: (page: Page<Expense>) => {
          this.expensesSubject.next(page.content);
          this.totalElements = page.totalElements;
        },
        error: (error) => console.log(error),
        complete: () => this.loadingSubject.next(false)
      });
  }
}
