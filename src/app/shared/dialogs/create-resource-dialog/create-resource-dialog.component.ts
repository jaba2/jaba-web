import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {Investment} from "../../../models/investment.interface";

@Component({
  selector: 'app-create-resource-dialog',
  templateUrl: './create-resource-dialog.component.html',
  styleUrls: ['./create-resource-dialog.component.sass']
})
export class CreateResourceDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CreateResourceDialogComponent>) { }

  ngOnInit(): void {
  }

  public newInvestment(investment: Investment): void {
    this.dialogRef.close(investment);
  }

}
