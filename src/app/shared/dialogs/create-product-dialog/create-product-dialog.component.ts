import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {Product} from "../../../models/product.interface";

@Component({
  selector: 'app-create-product-dialog',
  templateUrl: './create-product-dialog.component.html',
  styleUrls: ['./create-product-dialog.component.sass']
})
export class CreateProductDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CreateProductDialogComponent>) { }

  ngOnInit(): void {
  }

  public newProduct(product: Product): void {
    this.dialogRef.close(product);
  }

  public close(): void {
    this.dialogRef.close();
  }
}
