import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss']
})
export class AlertDialogComponent implements OnInit {

  constructor(private _dialogRef: MatDialogRef<AlertDialogComponent>) { }

  ngOnInit(): void {
  }

  public ok(): void {
    this._dialogRef.close(true);
  }
}
