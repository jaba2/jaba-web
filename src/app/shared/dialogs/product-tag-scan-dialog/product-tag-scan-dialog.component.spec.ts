import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTagScanDialogComponent } from './product-tag-scan-dialog.component';

describe('ProductTagScanDialogComponent', () => {
  let component: ProductTagScanDialogComponent;
  let fixture: ComponentFixture<ProductTagScanDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductTagScanDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductTagScanDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
