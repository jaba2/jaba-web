import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FormControl} from "@angular/forms";
import {ProductTag} from "../../../models/product-tag.interface";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AlertDialogComponent} from "../alert-dialog/alert-dialog.component";

@Component({
  selector: 'app-product-tag-scan-dialog',
  templateUrl: './product-tag-scan-dialog.component.html',
  styleUrls: ['./product-tag-scan-dialog.component.scss']
})
export class ProductTagScanDialogComponent implements OnInit {

  public groupSequence: number;
  public productTagScanForm: FormControl;
  public serialNumbers: Set<string>;
  public isChecked: boolean;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: number,
    private dialogRef: MatDialogRef<ProductTagScanDialogComponent>,
    private alertMatDialogRef: MatDialog,
    private snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.serialNumbers = new Set<string>();
    this.groupSequence = this.data;
    this.productTagScanForm = new FormControl('');
    this.isChecked = false;
  }

  public onEnter(): void {
    const serialNumber: string = this.productTagScanForm.value;
    if (!this.serialNumbers.has(serialNumber)) {
      this.serialNumbers.add(serialNumber);
      this.productTagScanForm = new FormControl('');
      if (Number(this.serialNumbers.size) === Number(this.groupSequence)) {
        this.dialogRef.close(this.buildProductTags());
      }
    } else {
      this.snackBar.open("Serial number already exists", "x");
    }
  }

  public onCheckChanges(): void {
    this.isChecked = true;
    const alertDialogRef = this.alertMatDialogRef.open(AlertDialogComponent, {
      width: '300px'
    });
    alertDialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        this.dialogRef.close(true)
      }
      this.isChecked = false;
    });
  }

  private buildProductTags(): ProductTag[] {
    const productTags: ProductTag[] = [];
    this.serialNumbers.forEach((serialNumber: string) => {
      productTags.push({serialNumber})
    });

    return productTags;
  }


}
