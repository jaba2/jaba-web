import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SaleDTO} from "../../../models/sale-dto.interface";
import {ProductSale} from "../../../models/ProductSale";
import {ProductTag} from "../../../models/product-tag.interface";

@Component({
  selector: 'app-sale-details-dialog',
  templateUrl: './sale-details-dialog.component.html',
  styleUrls: ['./sale-details-dialog.component.scss']
})
export class SaleDetailsDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SaleDTO,
    private dialogRef: MatDialogRef<SaleDetailsDialogComponent>,
  ) {
  }

  ngOnInit(): void {
  }

  public confirmSale(): void {
    this.dialogRef.close(true);
  }

  public getTotalPrice(): number {
    return this.data.productsSale
      .map((detail: ProductSale) => detail.totalPrice)
      .reduce((previous: number, total: number) => total + previous, 0);
  }

  public getProductTags(productTags: ProductTag[]): string {
    if (productTags && productTags.length > 0) {
      return productTags.map(productTag => productTag.serialNumber + "\n").toString();
    }

    return '';
  }

}
