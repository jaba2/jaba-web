import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleDetailsDialogComponent } from './sale-details-dialog.component';

describe('SaleDetailsDialogComponent', () => {
  let component: SaleDetailsDialogComponent;
  let fixture: ComponentFixture<SaleDetailsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaleDetailsDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SaleDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
