import {Component, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {ProductCodeService} from "../../../services/product-code/product-code.service";
import {ProductSale} from "../../../models/ProductSale";
import {JupiterTableBaseComponent} from "../../components/jupiter-table-base/jupiter-table-base.component";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ActionDelete} from "../../interfaces/action-delete.interface";
import {Product} from "../../../models/product.interface";
import {remove as _remove} from "lodash";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product-dialog.component.html',
  styleUrls: ['./search-product-dialog.component.scss']
})
export class SearchProductDialogComponent extends JupiterTableBaseComponent<ProductSale> implements OnInit  {
  // TODO
  // We need a end point to get the most 5 top find products
  // With these products, we can prebuild the auto complete form, to avoid the search
  // We need an endpoint to find by name LIKE
  // We need add filters by category and subcategory
  // it is not necessary a string to apply filters
  public displayedColumns = ['product', 'quantity', 'price', 'actions']

  public foundProductSales: Observable<ProductSale[]>;
  public searchCtrl: FormControl;
  public foundCodes: Set<String>;
  private lastPattern: string;

  constructor(
    private dialogRef: MatDialogRef<SearchProductDialogComponent>,
    private productCodeService: ProductCodeService,
              private dialog: MatDialog) {
    super(dialog);
  }

  ngOnInit(): void {
    this.initializeTable(this.displayedColumns);
    this.searchCtrl = new FormControl('');
    this.searchCtrl.valueChanges.subscribe((pattern: string) => {
      if(pattern && typeof pattern === 'string') {
        this.lastPattern = pattern;
        this.foundProductSales = this.productCodeService.getProductSales(pattern);
      }
    });
    this.foundCodes = new Set();
    this.lastPattern = '';
  }

  public addElements(): void {
    this.dialogRef.close(this.dataSource);
  }

  public onSearchChanges(event: MatAutocompleteSelectedEvent): void {
    this.searchCtrl.setValue(this.lastPattern);
    const code = event.option.value.productCode.code.toUpperCase();
    if(!this.foundCodes.has(code)) {
      this.dataSource.push(event.option.value);
      this.table.renderRows();
      this.foundCodes.add(code);
    }
  }

  public removeData(actionDelete: ActionDelete) {
    const dialogRef = this.showAlertDialog();
    dialogRef.afterClosed().subscribe(isDeleteAction => {
      if (isDeleteAction) {
        this.removeFromTable(actionDelete);
        this.table.renderRows();
      }
    });
  }

  private removeFromTable(actionDelete: ActionDelete): void {
    if (actionDelete.elementId) {
      _remove(this.dataSource, (productSale: ProductSale) => {
        return String(productSale.productCode) === String(actionDelete.elementId);
      });
    } else {
      this.dataSource.splice(actionDelete.index, 1);
    }
  }

}
