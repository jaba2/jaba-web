import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Subcategory} from "../../models/subcategory.interface";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/subcategory/`;
  }

  public getSubcategoriesByCategoryId(categoryId: number): Observable<Subcategory[]> {
    return this.http.get<Subcategory[]>(this.apiUrl + "category/" + categoryId);
  }
}
