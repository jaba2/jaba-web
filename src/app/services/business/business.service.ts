import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Business} from "../../models/Business";
import {Observable} from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/business/`;
  }

  public save(business: Business): Observable<Business> {
    return this.http.post<Business>(this.apiUrl, business);
  }

  public getAll(): Observable<Business[]> {
    return this.http.get<Business[]>(this.apiUrl);
  }
}
