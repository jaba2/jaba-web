import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Expense} from "../../models/expense.interface";
import {Page} from "../../models/paginator/page.interface";

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/expenses/`;
  }

  public save(expense: Expense): Observable<Expense> {
    return this.http.post<Expense>(this.apiUrl, expense);
  }

  public getAll(page: number, size: number, pattern: string, sort: string): Observable<Page<Expense>> {
    return this.http.get<Page<Expense>>(`${this.apiUrl}?page=${page}&size=${size}&pattern=${pattern}&sort=${sort}`);
  }
}
