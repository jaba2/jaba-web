import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {SaleDTO} from "../../models/sale-dto.interface";
import {Sale} from "../../models/sale.interface";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/sale/`;
  }

  public save(sale: SaleDTO): Observable<Sale> {
    return this.http.post<Sale>(this.apiUrl, sale);
  }

  public exportToPdf(saleId: number): Observable<void> {
    return this.http.get<void>(this.apiUrl + "export/" + saleId);
  }

}
