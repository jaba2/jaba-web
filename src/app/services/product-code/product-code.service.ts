import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProductCode} from "../../models/product-code.interface";
import {environment} from "../../../environments/environment";
import {ProductSale} from "../../models/ProductSale";

@Injectable({
  providedIn: 'root'
})
export class ProductCodeService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/product-codes/`;
  }

  public getProductCodes(): Observable<ProductCode[]> {
    return this.http.get<ProductCode[]>(this.apiUrl);
  }

  public getProductSales(pattern: string): Observable<ProductSale[]> {
    return this.http.get<ProductSale[]>(`${this.apiUrl}pattern/${pattern}`);
  }
}
