import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaymentType } from 'src/app/models/payment-type.interface';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class PaymentTypeService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/payment-type/`;
  }

  public getPaymentTypes(): Observable<PaymentType[]> {
    return this.http.get<PaymentType[]>(this.apiUrl);
  }
}
