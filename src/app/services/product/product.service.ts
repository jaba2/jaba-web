import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Vendor} from "../../models/vendor.interface";
import {Product} from "../../models/product.interface";
import {ProductSale} from "../../models/ProductSale";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.api}/product/`;
  }

  public findTopProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl +"top/");
  }

  public findByName(name: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl + "?name=" + name)
  }

  public findByCodeOrSku(codeOrSku: string): Observable<ProductSale> {
    return this.http.get<ProductSale>(this.apiUrl+ "scan/" + codeOrSku);
  }

}
