import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {
  SearchProductDialogComponent
} from "../../../shared/dialogs/search-product-dialog/search-product-dialog.component";
import {isEmpty as _isEmpty} from "lodash";
import {ProductSale} from "../../../models/ProductSale";
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../../../services/product/product.service";
import {BehaviorSubject} from "rxjs";
import {PaymentTypeService} from "../../../services/payment-type/payment-type.service";
import {PaymentType} from "../../../models/payment-type.interface";
import {SaleDetailsDialogComponent} from "../../../shared/dialogs/sale-details-dialog/sale-details-dialog.component";
import {SaleDTO} from "../../../models/sale-dto.interface";
import {SaleService} from "../../../services/sale/sale.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CustomerService} from "../../../services/customer/customer.service";
import {Customer} from "../../../models/customer.interface";
import {Sale} from "../../../models/sale.interface";
import {
  ProductTagScanDialogComponent
} from "../../../shared/dialogs/product-tag-scan-dialog/product-tag-scan-dialog.component";
import {ProductTag} from "../../../models/product-tag.interface";

@Component({
  selector: 'app-add-sale',
  templateUrl: './add-sale.component.html',
  styleUrls: ['./add-sale.component.scss']
})
export class AddSaleComponent implements OnInit {
  public displayedColumns: string[] = ['product', 'description', 'quantity',
    'totalQuantity', 'unitPrice', 'totalPrice', 'actions'];
  public dataSource: any;
  public scanSearchForm: FormControl;
  public saleForm: FormGroup;
  public paymentTypes: PaymentType[];
  public customers: Customer[];
  public isAddCustomer: Boolean;
  private scanCodes: Set<string>;
  private saleDTO: SaleDTO;


  constructor(
    private _dialog: MatDialog,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private paymentTypeService: PaymentTypeService,
    private saleService: SaleService,
    private customerService: CustomerService
  ) {
  }

  ngOnInit(): void {
    this.getPaymentTypes();
    this.getCustomers();
    this.initSaleForm();
    this.scanCodes = new Set();
    this.isAddCustomer = false;
  }

  get saleDetails(): FormArray {
    return this.saleForm.controls['saleDetails'] as FormArray;
  }

  public onEnter(): void {
    const code: string = this.scanSearchForm.value;
    if (code && !this.scanCodes.has(code.toUpperCase())) {
      this.getProductsByCodeOrSku(code);
    }
  }

  public removeRow(index: number) {
    const rowData = this.saleDetails?.at(index);
    this.saleDetails.removeAt(index);
    this.dataSource.next(this.saleDetails.controls);
    this.scanCodes.delete(rowData?.value.productCode.code);
  }

  public onCustomerAddChange(): void {
    this.isAddCustomer = true;
  }

  public onCustomerCancelChange(): void {
    this.isAddCustomer = false;
  }

  public getTotalCost(): number {
    return this.saleDetails.controls
      .map(row => row.getRawValue().totalPrice)
      .reduce((previous, total) => total + previous, 0);
  }

  public openSearchProductDialog(): void {
    const dialogRef = this._dialog.open(SearchProductDialogComponent, {
      disableClose: true,
      width: '500pt',
      height: '400pt'
    });
    dialogRef.afterClosed().subscribe(productSales => {
      if (productSales && !_isEmpty(productSales)) {
        productSales.forEach((productSale: ProductSale) => {
          if (!this.scanCodes.has(productSale.productCode.code)) {
            this.addRowToTable(productSale);
          }
        })
      }
    });
  }

  public openSaleDetailsDialog(): void {
    this.saleDTO = this.buildSaleDTO();
    const dialogRef = this._dialog.open(SaleDetailsDialogComponent, {
      width: '500pt',
      height: '400pt',
      data: this.saleDTO,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.addSale();
      }
    });
  }

  public openProductTagScanDialog(rowIndex: number): void {
    const rowData = this.saleDetails.at(rowIndex);
    const dialogRef = this._dialog.open(ProductTagScanDialogComponent, {
      width: '350px',
      height: '300px',
      data: rowData.getRawValue().quantity,
    });
    dialogRef.afterClosed().subscribe((result: ProductTag[] | boolean) => {
      if (result && result instanceof Object && Number(result.length) === Number(rowData.getRawValue().quantity)) {
        rowData.get('productTags')?.patchValue(result);
      }
      if (result === true) {
        rowData.get('productTags')?.patchValue([]);
        rowData.get('productTags')?.clearValidators();
        rowData.get('productTags')?.updateValueAndValidity();
      }
    });
  }

  private getProductsByCodeOrSku(codeOrSku: string): void {
    this.productService.findByCodeOrSku(codeOrSku).subscribe({
      next: (productSale: ProductSale) => this.addRowToTable(productSale),
      error: (error) => console.error(error)
    })
  }

  private addRowToTable(productSale: ProductSale): void {
    const row: FormGroup = this.buildRow(productSale);
    row.get('quantity')?.valueChanges.subscribe((value: number) => {
      row.controls['totalPrice'].setValue(value * row.controls['unitPrice'].value);
      this.resetProductTagsValidators(row);
    })
    row.get('unitPrice')?.valueChanges.subscribe((value: number) => {
      row.controls['totalPrice'].setValue(value * row.controls['quantity'].value);
    })
    this.saleDetails.push(row);
    this.dataSource.next(this.saleDetails.controls);
    this.scanCodes.add(productSale.productCode.code.toUpperCase());
  }

  private buildRow(productSale: ProductSale): FormGroup {
    return this.formBuilder.group({
      product: [{value: productSale.product, disabled: true}],
      description: [productSale.description, Validators.required],
      quantity: [productSale.quantity, [Validators.required, Validators.max(productSale.totalQuantity)]],
      totalQuantity: [{value: productSale.totalQuantity, disabled: true}],
      unitPrice: [productSale.unitPrice, Validators.required],
      totalPrice: [{value: productSale.totalPrice, disabled: true}],
      productCode: [productSale.productCode],
      sku: [productSale.sku],
      productTags: [null, Validators.required]
    })
  }

  private buildSaleDTO(): SaleDTO {
    return {
      description: this.saleForm.controls['description'].value,
      customer: this.getCustomer(),
      paymentType: this.saleForm.controls['paymentType'].value,
      productsSale: this.saleDetails.controls.map(row => row.getRawValue()),
    };
  }

  private getCustomer(): Customer {
    if(this.isAddCustomer) {
      return {
        name: this.saleForm.controls['customer'].value,
      };
    }

    return this.saleForm.controls['customer'].value;
  }

  private addSale(): void {
    if (this.isAddCustomer) {
      this.saveCustomer({ name: this.saleForm.controls['customer'].value});
    } else {
      this.saveSale();
    }
  }

  private reset(): void {
    this.scanCodes = new Set();
    this.initSaleForm();
  }

  private initSaleForm(): void {
    this.saleForm = this.formBuilder.group({
      saleDetails: this.formBuilder.array([]),
      description: [null, Validators.required],
      customer: [null, Validators.required],
      paymentType: [null, Validators.required],
    });
    this.dataSource = new BehaviorSubject<AbstractControl[]>([]);
    this.scanSearchForm = new FormControl('');
  }

  private resetProductTagsValidators(rowData: FormGroup): void {
    rowData.get('productTags')?.patchValue(null);
    rowData.get('productTags')?.setValidators(Validators.required);
    rowData.get('productTags')?.updateValueAndValidity();
  }

  private getPaymentTypes(): void {
    this.paymentTypeService.getPaymentTypes().subscribe({
      next: (paymentTypes: PaymentType[]) => this.paymentTypes = paymentTypes,
      error: (error) => console.error(error)
    });
  }

  private getCustomers(): void {
    this.customerService.getCustomers().subscribe({
      next: (customers: Customer[]) => this.customers = customers,
      error: (error) => console.error(error)
    });
  }

  private exportToPdf(saleId: number): void {
    this.saleService.exportToPdf(saleId).subscribe({
      next: () => this.snackBar.open("Ticket created", 'x'),
      error: (error) => console.error(error)
    });
  }

  private saveSale(): void {
    this.saleService.save(this.saleDTO).subscribe({
      next: (sale: Sale) => {
        this.exportToPdf(sale.saleId);
        this.snackBar.open("Sale save success with id " + sale.saleId, 'x');
        this.reset();
      },
      error: (error) => console.error(error),
    });
  }

  private saveCustomer(customer: Customer): void {
    this.customerService.save(customer).subscribe({
      next: (customer: Customer) => {
        this.saleDTO.customer = customer;
        this.saveSale();
      },
      error: (error) => console.error(error),
    });
  }
}
