import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Product} from "../../models/product.interface";
import {CreateProductDialogComponent} from "../../shared/dialogs/create-product-dialog/create-product-dialog.component";
import {ActionDelete} from "../../shared/interfaces/action-delete.interface";
import {JupiterTableBaseComponent} from "../../shared/components/jupiter-table-base/jupiter-table-base.component";
import {remove as _remove} from "lodash";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent extends JupiterTableBaseComponent<Product> implements OnInit {


  @Output() products: EventEmitter<Product[]> = new EventEmitter<Product[]>();
  @Input() initAddData: Boolean = false;

  public displayedColumns = ['name', 'code', 'currentQuantity', 'purchasePrice', 'category', 'subcategory'];

  constructor(private _dialog: MatDialog) {
    super(_dialog);
  }

  ngOnInit(): void {
    this.initializeTable(this.displayedColumns);
    if(this.initAddData) {
      this.addData();
    }
  }

  public addData() {
    const dialogRef =  this._dialog.open(CreateProductDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource.push(result);
        this.table.renderRows();
        this.products.emit(this.dataSource);
      }
    });
  }

  public removeData(actionDelete: ActionDelete) {
    const dialogRef = this.showAlertDialog();
    dialogRef.afterClosed().subscribe(isDeleteAction => {
      if (isDeleteAction) {
        this.removeFromTable(actionDelete);
        this.table.renderRows();
      }
    });
  }

  private removeFromTable(actionDelete: ActionDelete): void {
    if (actionDelete.elementId) {
      _remove(this.dataSource, (product: Product) => {
        return product.productId === actionDelete.elementId;
      });
      // TODO - do API request to delete
    } else {
      this.dataSource.splice(actionDelete.index, 1);
    }
  }



}
