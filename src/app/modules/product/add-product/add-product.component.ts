import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Product} from "../../../models/product.interface";
import {Category} from "../../../models/Category";
import {CategoryService} from "../../../services/category/category.service";
import {SubcategoryService} from "../../../services/subcategory/subcategory.service";
import {Subcategory} from "../../../models/subcategory.interface";
import {ProductFormValue} from "../../../models/product-form-value.interface";
import {ProductCodeService} from "../../../services/product-code/product-code.service";
import {ProductCode} from "../../../models/product-code.interface";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  @Output() newProduct: EventEmitter<Product> = new EventEmitter<Product>();

  public categoriesCatalog: Category[];
  public subcategoriesCatalog: Subcategory[];
  public productCodes: ProductCode[];
  public productFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _categoryService: CategoryService,
    private _subcategoryService: SubcategoryService,
    private productCodeService: ProductCodeService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.getCategories();
    this.getProductCodes();
    this.setProductCodeChanges()
    this.categoriesCatalog = [];
    this.subcategoriesCatalog = [];
  }

  public addNewProduct(productFormValue: ProductFormValue): void {
    this.newProduct.emit(this.buildProduct(productFormValue));
    this.initForm();
  }

  public categoriesOnChange(category: Category): void {
    const {
      categoryId
    } = category;

    this.getSubcategoriesByCategoryId(categoryId);
  }

  private setProductCodeChanges(): void {
    this.productFormGroup.get("productCode")?.valueChanges.subscribe((productCode: ProductCode) => {
      this.productFormGroup.get("name")?.setValue(productCode.description);
    })
  }

  private initForm(): void {
    this.productFormGroup = this._formBuilder.group({
      id: [null],
      name: [ {value: null, disabled: true}],
      productCode: [null, Validators.required],
      description: [null],
      initialQuantity: [null, Validators.required],
      purchasePrice: [null, Validators.required],
      suggestedPrice: [null, Validators.required],
      category: [null, Validators.required],
      subcategory: [null, Validators.required]
    });
  }

  private getCategories(): void {
    this._categoryService.getCategories().subscribe({
      next: (categories: Category[]) => {
        this.categoriesCatalog = categories;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  private getSubcategoriesByCategoryId(categoryId: number): void {
    this._subcategoryService.getSubcategoriesByCategoryId(categoryId).subscribe({
      next: (subcategories: Subcategory[]) => {
        this.subcategoriesCatalog = subcategories;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  private getProductCodes(): void {
    this.productCodeService.getProductCodes().subscribe({
      next: (productCodes: ProductCode[]) => this.productCodes = productCodes,
      error: (error) => console.error(error)
    });
  }


  private buildProduct(productFormValue: ProductFormValue): Product {
    return {
      name: this.productFormGroup.getRawValue().name,
      productCode: productFormValue.productCode,
      description: productFormValue.description,
      initialQuantity: productFormValue.initialQuantity,
      quantity: productFormValue.initialQuantity,
      purchasePrice: productFormValue.purchasePrice,
      suggestedPrice: productFormValue.suggestedPrice,
      subcategory: {
        subcategoryId: productFormValue.subcategory.subcategoryId,
        name: productFormValue.subcategory.name,
        description: productFormValue.subcategory.description,
        category: productFormValue.category,
      }
    }
  }


}
