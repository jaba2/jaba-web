import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Investment} from "../../models/investment.interface";
import {
  CreateResourceDialogComponent
} from "../../shared/dialogs/create-resource-dialog/create-resource-dialog.component";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ActionDelete} from "../../shared/interfaces/action-delete.interface";
import {JupiterTableBaseComponent} from "../../shared/components/jupiter-table-base/jupiter-table-base.component";
import {remove as _remove} from "lodash";

@Component({
  selector: 'app-investment',
  templateUrl: './investment.component.html',
  styleUrls: ['./investment.component.scss']
})
export class InvestmentComponent extends JupiterTableBaseComponent<Investment> implements OnInit, OnChanges {

  @Output() public investments: EventEmitter<Investment[]> = new EventEmitter<Investment[]>();
  @Input() initAddData: Boolean = false;
  public displayedColumns = ['name', 'description', 'amount', 'paymentType']

  constructor(private _dialog: MatDialog) {
    super(_dialog);
  }

  ngOnInit(): void {
    this.initializeTable(this.displayedColumns);
    if (this.initAddData) {
      this.addData();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.initAddData && changes['initAddData'] && changes['initAddData'].currentValue) {
      this.addData();
    }
  }

  public addData() {
    const dialogRef = this._dialog.open(CreateResourceDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource.push(result);
        this.table.renderRows();
        this.investments.emit(this.dataSource);
      }
    });
  }

  public removeData(actionDelete: ActionDelete) {
    const dialogRef = this.showAlertDialog();
    dialogRef.afterClosed().subscribe(isDeleteAction => {
      if (isDeleteAction) {
        this.removeFromTable(actionDelete);
        this.table.renderRows();
      }
    });
  }

  private removeFromTable(actionDelete: ActionDelete): void {
    if (actionDelete.elementId) {
      _remove(this.dataSource, (investment: Investment) => {
        return investment.investmentId === actionDelete.elementId;
      });
      // TODO - do API request to delete
    } else {
      this.dataSource.splice(actionDelete.index, 1);
    }
  }

}
