import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Investment} from 'src/app/models/investment.interface';
import {PaymentType} from 'src/app/models/payment-type.interface';
import {PaymentTypeService} from 'src/app/services/payment-type/payment-type.service';

@Component({
  selector: 'app-add-investment',
  templateUrl: './add-investment.component.html',
  styleUrls: ['./add-investment.component.scss'],
})
export class AddInvestmentComponent implements OnInit {
  public paymentTypesCatalog: PaymentType[];
  public investmentFormGroup: FormGroup;

  @Output() newInvestment: EventEmitter<Investment> = new EventEmitter<Investment>();

  constructor(
    private _formBuilder: FormBuilder,
    private paymentTypeService: PaymentTypeService
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.getPaymentTypes();
  }

  public addNewInvestment(investment: Investment): void {
    this.newInvestment.emit(investment);
    this.initForm();
  }

  private getPaymentTypes(): void {
    this.paymentTypeService.getPaymentTypes().subscribe({
      next: (paymentTypes: PaymentType[]) => {
        this.paymentTypesCatalog = paymentTypes;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  private initForm(): void {
    this.investmentFormGroup = this._formBuilder.group({
      id: [Date.now()],
      amount: [null, Validators.required],
      name: [null, [Validators.required, Validators.minLength(4)]],
      description: [null],
      paymentType: [null, Validators.required],
    });
  }
}
