import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

import {MatTable} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";

import {Business} from "../../models/Business";
import {PageTitleTypes} from "../../shared/constants/page-title-types.enum";
import {Sort} from "@angular/material/sort";
import {BusinessService} from "../../services/business/business.service";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BusinessComponent implements OnInit {

  @ViewChild(MatTable) table: MatTable<Business>;
  @Input() public dataSource: Business[];

  public displayedColumns = ['name', 'description', 'investmentAmount', 'created', 'vendor'];
  public columnsToDisplayWithExpand = [...this.displayedColumns, 'expand'];
  public pageTitles = ['Businesses', 'New business']
  public isAddingBusiness: boolean;
  public expandedElement: Business | null;

  constructor(private _dialog: MatDialog,
              private businessService: BusinessService,
              private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.dataSource = [];
    this.getBusiness();
    this.isAddingBusiness = false;
  }

  public addData(): void {
    this.isAddingBusiness = true;

  }

  public newBusiness(business: Business): void {
    this.isAddingBusiness = false;
    this.dataSource.push(business);
    this.table.renderRows();
  }

  public cancel(): void {
    this.isAddingBusiness = false;
  }

  public getColumnName(columnContent: any, column: string): string  {
    if (columnContent instanceof Object) {
      return columnContent.name
    }
    if (column === "created") {
      const dateFormatted =  this.datePipe.transform(columnContent,'MMM d, y');
      if (dateFormatted) {
        return dateFormatted
      }
    }
    return columnContent
  }

  public getPageTitle(): string {
    if (this.isAddingBusiness) {
      return this.pageTitles[PageTitleTypes.NEW];
    }
    return this.pageTitles[PageTitleTypes.VIEW];
  }

  public sortData(sort: Sort) {
    console.log(sort)
    if (!sort.active || sort.direction === '') {
      this.dataSource = this.dataSource;
      return;
    }
    this.dataSource = this.dataSource.sort((a: Business, b: Business) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return this.compare(a.name, b.name, isAsc);
        case 'description':
          return this.compare(a.description, b.description, isAsc);
        case 'investmentAmount':
          return this.compare(a.investmentAmount ? a.investmentAmount : 0, b.investmentAmount ? b.investmentAmount : 0, isAsc);
        case 'createdDate':
          return this.compare(a.created ? a.created : '', b.created ? b.created : '', isAsc);
        case 'vendor':
          return this.compare(a.vendor.name, b.vendor.name, isAsc);
        default:
          return 0;
      }
    });
    this.table.renderRows();
  }

  private compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  private getBusiness(): void {
    this.businessService.getAll().subscribe({
      next: (businesses: Business[]) => this.dataSource = businesses,
      error: (error) => console.error(error),
    });

  }

}
