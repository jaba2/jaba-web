import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Investment} from "../../../models/investment.interface";
import {Product} from "../../../models/product.interface";
import {Vendor} from "../../../models/vendor.interface";
import {VendorService} from "../../../services/vendor/vendor.service";
import {Business} from "../../../models/Business";
import {BusinessService} from "../../../services/business/business.service";

@Component({
  selector: 'app-add-business',
  templateUrl: './add-business.component.html',
  styleUrls: ['./add-business.component.scss']
})
export class AddBusinessComponent implements OnInit {

  @Output() newBusiness: EventEmitter<Business> = new EventEmitter<Business>();

  public businessFormGroup: FormGroup;
  public investments: Investment[];
  public investmentsFormGroup: FormGroup;
  public products: Product[];
  public productsFormGroup: FormGroup;
  public vendors: Vendor[];
  public isAddData: boolean;
  public isLinear: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private _vendorService: VendorService,
    private _businessService: BusinessService,
  ) {
  }

  ngOnInit(): void {
    this.initForms();
    this.getVendors();
    this.initArrays();
    this.isAddData = false;
    this.isLinear = true;
  }

  public saveBusiness(): void {
    const business: Business = this.buildBusiness();
    this._businessService.save(business).subscribe({
      next: (business: Business) => {
        this.newBusiness.emit(business);
      },
      error: (error) => console.error(error),
    });
  }

  public addInvestments(investments: Investment[]): void {
    this.investmentsFormGroup.controls['investments'].patchValue(investments);
  }

  public addProducts(products: Product[]): void {
    this.productsFormGroup.controls['products'].patchValue(products);
  }

  public isSubmitBusinessDisabled(): boolean {
    return (this.businessFormGroup.invalid ||
      this.investmentsFormGroup.invalid ||
      this.productsFormGroup.invalid);
  }

  public onInvestmentsStepChange(event: any): void {
    console.log(event);
    if (this.investmentsFormGroup.invalid) {
      this.isAddData = true;
    }
  }

  private initForms(): void {
    this.businessFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      description: [''],
      vendor: [null, Validators.required],
    });

    this.investmentsFormGroup = this._formBuilder.group({
      investments: [null, Validators.required],
    });

    this.productsFormGroup = this._formBuilder.group({
      products: [null, Validators.required],
    });
  }

  private getVendors(): void {
    this._vendorService.getVendors().subscribe({
      next: (vendors: Vendor[]) => {
        this.vendors = vendors;
      },
      error: (error) => console.error(error),
    });
  }

  private initArrays(): void {
    this.investments = [];
    this.products = [];
  }

  private buildBusiness(): Business {
    return {
      name: this.businessFormGroup.value.name,
      description: this.businessFormGroup.value.description,
      vendor: this.businessFormGroup.value.vendor,
      investments: this.investmentsFormGroup.value.investments,
      products: this.productsFormGroup.value.products
    }
  }

}
