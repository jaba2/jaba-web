import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PaymentType} from "../../../models/payment-type.interface";
import {ExpenseCategory} from "../../../models/expense-category.interface";
import {ExpenseCategoryService} from "../../../services/expense-category/expense-category.service";
import {ExpenseService} from "../../../services/expense/expense.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Expense} from "../../../models/expense.interface";
import {PaymentTypeService} from "../../../services/payment-type/payment-type.service";

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.scss']
})
export class AddExpenseComponent implements OnInit {

  @Output() newExpense: EventEmitter<Expense> = new EventEmitter<Expense>();

  public expenseForm: FormGroup;
  public expenseCategories: ExpenseCategory[];
  public paymentTypes: PaymentType[];

  constructor(
    private fb: FormBuilder,
    private expenseCategoryService: ExpenseCategoryService,
    private expenseService: ExpenseService,
    private paymentTypeService: PaymentTypeService,
    private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.initForm();
    this.getAllExpenseCategories();
    this.getAllPaymentTypes();
  }

  public addData(expense: Expense) {
    this.saveExpense(expense);
  }

  private initForm(): void {
    this.expenseForm = this.fb.group({
      amount: [null, Validators.required],
      name: [null, Validators.required],
      expenseCategory: [null, Validators.required],
      paymentType: [null, Validators.required]
    });
  }

  private getAllExpenseCategories(): void {
    this.expenseCategoryService.getAll().subscribe({
      next: (expenseCategories: ExpenseCategory[]) => this.expenseCategories = expenseCategories,
      error: (error) => console.error(error),
    });
  }

  private getAllPaymentTypes(): void {
    this.paymentTypeService.getPaymentTypes().subscribe({
      next: (paymentTypes: PaymentType[]) => this.paymentTypes = paymentTypes,
      error: (error) => console.error(error)
    })
  }

  private saveExpense(expense: Expense): void {
    this.expenseService.save(expense).subscribe({
      next: (expense: Expense) => {
        this.initForm();
        this.snackBar.open("Expense saved!", "x");
        this.newExpense.emit(expense);
      },
      error: (error) => console.error(error),
    });
  }
}
