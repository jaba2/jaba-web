import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DatePipe} from "@angular/common";
import {PageTitleTypes} from "../../shared/constants/page-title-types.enum";
import {MatSort} from "@angular/material/sort";
import {ExpenseService} from "../../services/expense/expense.service";
import {Expense} from "../../models/expense.interface";
import {MatPaginator} from "@angular/material/paginator";
import {Page} from "../../models/paginator/page.interface";
import {debounceTime, distinctUntilChanged, fromEvent, merge, tap} from "rxjs";
import {ExpenseDataSource} from "../../shared/classes/expense-datasource.class";

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  public dataSource: ExpenseDataSource;
  public displayedColumns = ['name', 'amount', 'expenseCategory', 'paymentType', 'created'];
  public pageTitles = ['Expenses', 'New Expense'];
  public pageSizeOptions = [5, 10, 25, 100, 500];
  public isAddingExpense: boolean;
  public isLoading: boolean;
  public pageSize: number;
  public currentPage: number;
  public page: Page<Expense>;

  constructor(private _dialog: MatDialog,
              private expenseService: ExpenseService,
              private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.isAddingExpense = false;
    this.isLoading = false;
    this.pageSize = 25;
    this.currentPage = 0;
    this.dataSource = new ExpenseDataSource(this.expenseService);
    this.dataSource.loadExpenses(this.currentPage, this.pageSize, '', 'created,desc');
  }

  ngAfterViewInit(): void {

    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadExpensesPage()
        })
      )
      .subscribe();

    this.paginator.page
      .pipe(
        tap(() => this.loadExpensesPage())
      )
      .subscribe();

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadExpensesPage())
      )
      .subscribe();
  }

  public addData(): void {
    this.isAddingExpense = true;
  }

  public newExpense(expense: Expense): void {
    this.isAddingExpense = false;
    this.dataSource.add(expense);
  }

  public cancel(): void {
    this.isAddingExpense = false;
  }

  public getColumnName(columnContent: any, column: string): string {
    if (columnContent instanceof Object) {
      return columnContent.name
    }
    if (column === "created") {
      const dateFormatted = this.datePipe.transform(columnContent, 'MMM d, y');
      if (dateFormatted) {
        return dateFormatted
      }
    }
    return columnContent
  }

  public getPageTitle(): string {
    if (this.isAddingExpense) {
      return this.pageTitles[PageTitleTypes.NEW];
    }
    return this.pageTitles[PageTitleTypes.VIEW];
  }

  private loadExpensesPage(): void {
    this.dataSource.loadExpenses(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.input.nativeElement.value,
      `${this.sort.active},${this.sort.direction}`);
  }
}
